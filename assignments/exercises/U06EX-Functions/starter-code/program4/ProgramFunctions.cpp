#include "ProgramFunctions.h"

#include <iostream>
using namespace std;

void DisplayMainMenu() {
  cout << "I. Init character" << endl;
  cout << "E. Edit character" << endl;
  cout << "L. Load character" << endl;
  cout << "S. Save character" << endl;
  cout << "X. Exit" << endl;
  cout << endl;
}

char GetCharChoice() {
  char choice;
  cout << "Choice: ";
  cin >> choice;
  choice = toupper(choice);
  return choice;
}