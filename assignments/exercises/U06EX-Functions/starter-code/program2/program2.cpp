#include <array>    /* Use arrays */
#include <fstream>  /* Use ifstream */
#include <iostream> /* Use cout */
using namespace std;

#include "ArrayTools.h"

int main(int argumentCount, char *argumentList[]) {
  /* Check for enough arguments */
  if (argumentCount < 2) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << argumentList[0] << " inputfile" << endl;
    return 1; /* Exit with error code 1 */
  }

  array<float, 10> list;

  // Add code here

  /* Quit program with code 0 (no errors) */
  return 0;
}