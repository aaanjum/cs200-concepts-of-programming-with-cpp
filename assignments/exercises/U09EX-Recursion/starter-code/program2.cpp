#include <iostream> /* Use cout */
using namespace std;

int SumUp_Iter(int start, int end) {
}

int SumUp_Rec(int start, int end) {
}

int main(int argumentCount, char *argumentList[]) {
  /* Check for enough arguments */
  if (argumentCount < 3) {
    cout << "NOT ENOUGH ARGUMENTS" << endl;
    cout << "Expected form: " << argumentList[0] << " start end" << endl;
    return 1; /* Exit with error code 1 */
  }

  int start = stoi(argumentList[1]);
  int end = stoi(argumentList[2]);
  int result;

  cout << endl << "ITERATIVE VERSION: ";
  result = SumUp_Iter(start, end);
  cout << result << endl;

  cout << endl << "RECURSIVE VERSION: ";
  result = SumUp_Rec(start, end);
  cout << result << endl;

  /* Quit program with code 0 (no errors) */
  cout << endl << "GOODBYE" << endl;
  return 0;
}
