clear
echo "-------------------------------------------------"
echo "Build program 1..."
clang++ program1.cpp -o program1exe
echo "-------------------------------------------------"
echo "TEST 1: ./program1exe 1 5"
echo ""
echo "EXPECTED OUTPUT:"
echo "ITERATIVE: 1 2 3 4 5"
echo "RECURSIVE: 1 2 3 4 5"
echo ""
echo "ACTUAL OUTPUT:"
./program1exe 1 5
echo ""
echo "-------------------------------------------------"
echo "TEST 1: ./program1exe 5 10"
echo ""
echo "EXPECTED OUTPUT:"
echo "ITERATIVE: 5 6 7 8 9 10"
echo "RECURSIVE: 5 6 7 8 9 10"
echo ""
echo "ACTUAL OUTPUT:"
./program1exe 5 10
echo ""
echo "-------------------------------------------------"
echo "CODE:"
cat program1.cpp

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 2..."
clang++ program2.cpp -o program2exe
echo "-------------------------------------------------"
echo "TEST 1: ./program2exe 1 5"
echo ""
echo "EXPECTED OUTPUT:"
echo "ITERATIVE: 15"
echo "RECURSIVE: 15"
echo ""
echo "ACTUAL OUTPUT:"
./program2exe 1 5
echo ""
echo "-------------------------------------------------"
echo "TEST 1: ./program1exe 5 10"
echo ""
echo "EXPECTED OUTPUT:"
echo "ITERATIVE: 45"
echo "RECURSIVE: 45"
echo ""
echo "ACTUAL OUTPUT:"
./program2exe 5 10
echo ""
echo "-------------------------------------------------"
echo "CODE:"
cat program2.cpp

echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "Build program 3..."
clang++ program3.cpp -o program3exe
echo "-------------------------------------------------"
echo "TEST 1: ./program3exe"
echo ""
echo "EXPECTED OUTPUT:"
echo "ITERATIVE: 15"
echo "RECURSIVE: 15"
echo ""
echo "ACTUAL OUTPUT:"
./program3exe

echo ""
echo "-------------------------------------------------"
echo "CODE:"
#cat program3.cpp
